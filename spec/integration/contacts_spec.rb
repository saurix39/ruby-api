# spec/integration/pets_spec.rb
require 'swagger_helper'

describe 'Contacts API' do

  path '/contacts' do

    get 'Returns all contacts' do
      tags 'Contacts'
      produces 'application/json'

      response '200', 'OK' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string },
              last_name: { type: :string },
              email: { type: :string },
              address: { type: :string },
              house_type: { type: :string },
              description: { type: :string },
              birthdate: { type: :string, format: :date },
              gender_id: { type: :integer },
              city_id: { type: :integer },
              created_at: { type: :string, format: 'date-time' },
              updated_at: { type: :string, format: 'date-time' },
            },
            required: [ 'id', 'name', 'last_name', 'email', 'address', 'house_type', 'birthdate', 'gender_id', 'city_id', 'created_at', 'updated_at' ]
          }
        run_test!
      end

      response '404', 'Not Found' do
        run_test!
      end
    end

    post 'Creates a contact' do
      tags 'Contacts'
      consumes 'application/json'
      parameter name: :contact, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          last_name: { type: :string },
          email: { type: :string },
          address: { type: :string },
          house_type: { type: :string },
          description: { type: :string },
          birthdate: { type: :string, format: :date },
          gender_id: { type: :integer },
          city_id: { type: :integer }
        },
        required: [ 'name', 'last_name', 'email', 'address', 'house_type', 'birthdate', 'gender_id', 'city_id' ]
      }

      response '201', 'contact created' do
        let(:contact) { { name: 'John', last_name: 'Doe', email: 'john.doe@example.com', address: '123 Main St.', house_type: 'apartment', description: 'Lorem ipsum dolor sit amet', birthdate: '02/02/2000', gender_id: 1, city_id: 1 } }
        run_test!
      end

      response '400', 'bad request' do
        let(:contact) { { name: 'John', last_name: 'Doe', email: 'john.doe@example.com', address: '123 Main St.', house_type: 'apartment', description: 'Lorem ipsum dolor sit amet', birthdate: '02/02/2000', gender_id: 1 } }
        run_test!
      end

      response '422', 'unprocessable entity' do
        let(:contact) { { name: 'John', last_name: 'Doe', email: 'john.doe@example.com', address: '123 Main St.', house_type: 'apartment', description: 'Lorem ipsum dolor sit amet', birthdate: '02/02/2000', gender_id: 1, city_id: 1 } }
        before do |example|
          allow_any_instance_of(Contact).to receive(:save).and_return(false)
        end
        run_test!
      end
    end
  end

  path '/contacts/country/count' do
    get 'Returns the number of contacts per city and country' do
      tags 'Contacts'
      produces 'application/json'
      response '200', 'OK' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              city_id: { type: :integer },
              name: { type: :string },
              country_name: { type: :string },
              total_records_in_contacts: { type: :integer }
            },
            required: [ 'city_id', 'name', 'country_name', 'total_records_in_contacts' ]
          }
        run_test!
      end
    end
  end

  path '/contacts/{id}' do

    get 'Retrieves a contact' do
      tags 'Contacts'
      produces 'application/json'
      parameter name: :id, in: :path, type: :integer

      response '200', 'contact found' do
        let(:id) { Contact.create(name: 'John', last_name: 'Doe', email: 'johndoe@example.com', gender_id: 1, city_id: 1).id }
        run_test!
      end

      response '404', 'contact not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end

    put 'Updates a contact' do
      tags 'Contacts'
      consumes 'application/json', 'application/xml'
      parameter name: :id, in: :path, type: :integer
      parameter name: :contact, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          last_name: { type: :string },
          email: { type: :string },
          address: { type: :string },
          house_type: { type: :string },
          description: { type: :string },
          birthdate: { type: :string, format: :date },
          gender_id: { type: :integer },
          city_id: { type: :integer }
        },
        required: [ 'name', 'last_name', 'email', 'gender_id', 'city_id' ]
      }

      response '200', 'contact updated' do
        let(:id) { Contact.create(name: 'foo', last_name: 'bar', email: 'foo@bar.com', gender_id: 1, city_id: 1).id }
        let(:contact) { { name: 'baz' } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:id) { Contact.create(name: 'foo', last_name: 'bar', email: 'foo@bar.com', gender_id: 1, city_id: 1).id }
        let(:contact) { { email: nil } }
        run_test!
      end
    end

    delete 'Deletes a contact' do
      tags 'Contacts'
      produces 'application/json', 'application/xml'
      parameter name: :id, in: :path, type: :integer

      response '204', 'contact deleted' do
        let(:id) { Contact.create(name: 'foo', last_name: 'bar', email: 'foo@bar.com', gender_id: 1, city_id: 1).id }
        run_test!
      end

      response '404', 'contact not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end

  path '/countries' do

    post 'Creates a country' do
      tags 'Countries'
      consumes 'application/json'
      parameter name: :country, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string }
        },
        required: [ 'name' ]
      }

      response '201', 'country created' do
        let(:country) { { name: 'Spain' } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:country) { { name: '' } }
        run_test!
      end
    end

    get 'Retrieves all countries' do
      tags 'Countries'
      produces 'application/json'

      response '200', 'countries found' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string }
            },
            required: [ 'id', 'name' ]
          }

        run_test!
      end
    end
  end

  path '/countries/{id}' do

    get 'Retrieves a country' do
      tags 'Countries'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'country found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string }
          },
          required: [ 'id', 'name' ]

        let(:id) { Country.create(name: 'Spain').id }
        run_test!
      end

      response '404', 'country not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end

    patch 'Updates a country' do
      tags 'Countries'
      consumes 'application/json'
      parameter name: :id, :in => :path, :type => :integer
      parameter name: :country, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string }
        },
        required: [ 'name' ]
      }

      response '200', 'country updated' do
        let(:id) { Country.create(name: 'Spain').id }
        let(:country) { { name: 'Portugal' } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:id) { Country.create(name: 'Spain').id }
        let(:country) { { name: '' } }
        run_test!
      end
    end

    delete 'Deletes a country' do
      tags 'Countries'
      consumes 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'country deleted' do
        let(:id) { Country.create(name: 'Spain').id }
        run_test!
      end

      response '404', 'country not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end

  path '/cities' do

    get 'Retrieves all cities' do
      tags 'Cities'
      produces 'application/json'

      response '200', 'cities found' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string },
              country_id: { type: :integer }
            },
            required: [ 'id', 'name', 'country_id' ]
          }

        run_test!
      end
    end

    post 'Creates a city' do
      tags 'Cities'
      consumes 'application/json'
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          country_id: { type: :integer }
        },
        required: [ 'name', 'country_id' ]
      }

      response '201', 'city created' do
        let(:city) { { name: 'New City', country_id: 1 } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:city) { { name: nil } }
        run_test!
      end
    end
  end

  path '/cities/{id}' do

    parameter name: :id, in: :path, type: :integer

    get 'Retrieves a city' do
      tags 'Cities'
      produces 'application/json'

      response '200', 'city found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            country_id: { type: :integer }
          },
          required: [ 'id', 'name', 'country_id' ]

        let(:id) { City.create(name: 'New City', country_id: 1).id }
        run_test!
      end

      response '404', 'city not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end

    put 'Updates a city' do
      tags 'Cities'
      consumes 'application/json'
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          country_id: { type: :integer }
        },
        required: [ 'name', 'country_id' ]
      }

      response '200', 'city updated' do
        let(:city) { { name: 'Updated City', country_id: 2 } }
        let(:id) { City.create(name: 'New City', country_id: 1).id }
        run_test!
      end

      response '422', 'invalid request' do
        let(:city) { { name: nil } }
        let(:id) { City.create(name: 'New City', country_id: 1).id }
        run_test!
      end
    end

    delete 'Deletes a city' do
      tags 'Cities'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'city deleted' do
        let(:city) { create(:city) }
        let(:id) { city.id }
        run_test! do
          expect(City.exists?(id)).to be_falsey
        end
      end

      response '404', 'city not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end





  path '/genders' do

    get 'Retrieves all genders' do
      tags 'Genders'
      produces 'application/json'

      response '200', 'genders found' do
        schema type: :array,
          items: {
            type: :object,
            properties: {
              id: { type: :integer },
              name: { type: :string }
            },
            required: [ 'id', 'name' ]
          }

        run_test!
      end
    end

    post 'Creates a gender' do
      tags 'Genders'
      consumes 'application/json'
      parameter name: :city, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string }
        },
        required: [ 'name' ]
      }

      response '201', 'gender created' do
        let(:gender) { { name: 'New Gender'} }
        run_test!
      end

      response '422', 'invalid request' do
        let(:gender) { { name: nil } }
        run_test!
      end
    end
  end

  path '/genders/{id}' do

    parameter name: :id, in: :path, type: :integer

    get 'Retrieves a gender' do
      tags 'Genders'
      produces 'application/json'

      response '200', 'gender found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string }
          },
          required: [ 'id', 'name' ]

        let(:id) { Gender.create(name: 'New Gender').id }
        run_test!
      end

      response '404', 'gender not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end

    put 'Updates a gender' do
      tags 'Genders'
      consumes 'application/json'
      parameter name: :gender, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string }
        },
        required: [ 'name' ]
      }

      response '200', 'gender updated' do
        let(:gender) { { name: 'Updated Gender', country_id: 2 } }
        let(:id) { Gender.create(name: 'New Gender').id }
        run_test!
      end

      response '422', 'invalid request' do
        let(:gender) { { name: nil } }
        let(:id) { Gender.create(name: 'New Gender').id }
        run_test!
      end
    end

    delete 'Deletes a gender' do
      tags 'Genders'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '204', 'gender deleted' do
        let(:gender) { create(:gender) }
        let(:id) { gender.id }
        run_test! do
          expect(Gender.exists?(id)).to be_falsey
        end
      end

      response '404', 'gender not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end