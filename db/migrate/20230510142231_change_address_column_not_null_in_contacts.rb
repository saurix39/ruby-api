class ChangeAddressColumnNotNullInContacts < ActiveRecord::Migration[7.0]
  def change
    change_column :contacts, :address, :string, null: false
  end
end
