class ChangeNameColumnNotNullInCities < ActiveRecord::Migration[7.0]
  def change
    change_column :cities, :name, :string, null: false
  end
end
