class AddGenderToContacts < ActiveRecord::Migration[7.0]
  def change
    add_reference :contacts, :gender, null: false, foreign_key: true
  end
end
