class RemoveCountryFkFromCities < ActiveRecord::Migration[7.0]
  def change
    remove_column :cities, :country_fk, :integer
  end
end
