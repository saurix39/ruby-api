class CreateContacts < ActiveRecord::Migration[7.0]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :address
      t.string :house_type
      t.string :description
      t.date :birthdate
      t.integer :gender_fk
      t.integer :city_fk
      t.timestamps
    end
  end
end
