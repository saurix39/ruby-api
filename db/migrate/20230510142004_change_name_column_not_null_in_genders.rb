class ChangeNameColumnNotNullInGenders < ActiveRecord::Migration[7.0]
  def change
    change_column :genders, :name, :string, null: false
  end
end
