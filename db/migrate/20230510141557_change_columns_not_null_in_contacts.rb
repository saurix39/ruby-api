class ChangeColumnsNotNullInContacts < ActiveRecord::Migration[7.0]
  def change
    change_column :contacts, :name, :string, null: false
    change_column :contacts, :last_name, :string, null: false
    change_column :contacts, :email, :string, null: false
    change_column :contacts, :address, :string, null: false
    change_column :contacts, :house_type, :string, null: false
    change_column :contacts, :description, :string, null: false
    change_column :contacts, :birthdate, :date, null: false
  end
end
