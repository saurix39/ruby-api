Rails.application.routes.draw do
  mount Rswag::Api::Engine => '/api-docs'
  mount Rswag::Ui::Engine => 'api-docs'
  resources :contacts
  resources :cities
  resources :genders
  resources :countries
  get '/contacts/country/count', to: 'contacts#country_count'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
