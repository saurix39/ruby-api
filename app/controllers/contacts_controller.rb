class ContactsController < ApplicationController
  before_action :set_contact, only: %i[ show update destroy ]

  # GET /contacts
  def index
    @contacts = Contact.all

    render json: @contacts
  end

  # GET /contacts/country/count
  def country_count
    @registers_by_city = Contact.joins(city: :country)
                            .select("city_id, cities.name, countries.name as country_name,  count(*) as total_records_in_contacts")
                            .group(:city_id)
                            .as_json(except: :id)
    render json: @registers_by_city
  end

  # GET /contacts/1
  def show
    render json: @contact
  end

  # POST /contacts
  def create
    @total_records_in_city = Contact.where(city_id: contact_params[:city_id]).count
    if @total_records_in_city >= 3
      return render json: {error: "Solo son permitidos 3 registros por ciudad"}, status: 400
    end
    @contact = Contact.new(contact_params)
    if @contact.save
      render json: @contact, status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /contacts/1
  def update
    if @contact.update(contact_params)
      render json: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contacts/1
  def destroy
    @contact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contact_params
      params.require(:contact).permit(:name, :last_name, :email, :address, :house_type, :description, :birthdate, :gender_id, :city_id)
    end
end
