class Contact < ApplicationRecord
    belongs_to :gender
    belongs_to :city
    validates_date :birthdate,
        :on_or_before => lambda { 18.years.ago },
        :on_or_before_message => "Debes tener al menos 18 años para registrarte",
        :invalid_date_message => "La fecha no es válida",
        :date_format => :short
end
