# Contacts API
##### Swagger api documentation in http://localhost:3000/api-docs/index.html

This project corresponds to an api created in ruby ​​on rails which has the ability to view, create, edit and delete contact records. In the same way, it also has the same functionalities for countries, cities and genders.

## Requirements

- Ruby 2.7.2.
- Rails 7.0.4
- MySQL 14.14 Distrib 5.7.42, for Linux (x86_64).
- Developed on Ubuntu Desktop 18.04.6 LTS operating system.
## Project installation
- First install the indicated version of ruby ​​using RVM or any version manager of your choice.
- Create and use a gemset where all dependencies for this project can be hosted, preferably using RVM.
For RVM use the following commands
   * rvm use 2.7.2.
   * rvm gemset create gemset_name
   * rvm gemset use gemset_name
- For the installation of certain dependencies it is recommended to have git installed, [click here](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git) to see its installation in ubuntu.
- Once everything is ready we proceed to give it the following command to install the dependencies within the gemset.
    * bundle install 
## Consider!!
- In some cases the installation of the mysql2 gem can generate an error, so if this error occurs when installing it, execute the following command. [Solution found here](https://stackoverflow.com/questions/3608287/error-installing-mysql2-failed-to-build-gem-native-extension)
    * sudo apt-get install libmysqlclient-dev

## Installation continued
- Within the root of the project, create an .env file where the corresponding credentials for the database will be found. Set the following environment variables.

| env name | env description |
|----------|-----------------|
| DATABASE_NAME | Name of the database to use |
| DATABASE_USERNAME | Name of the user who has access to the database along with all its tables |
| DATABASE_PASSWORD | Password of the user who has access to the database |

## Database configuration
- Once MySQL is installed you must create the database for the project, use the following steps for it.
    * Open MySQL with: sudo mysql -u root -p
    * Create the database with: create database <database_name>;
    * Use the database with: use <database_name>;
- Once the database is created you can create a user that has access to all the tables in the database, for which you can use the following steps [(Process taken from)](https://www.digitalocean.com/community/tutorials/crear-un-nuevo-usuario-y-otorgarle-permisos-en-mysql-es):
    * CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
    * GRANT ALL PRIVILEGES ON <database_name> . * TO 'newuser'@'localhost';
    * FLUSH PRIVILEGES;
## Final steps
- Inside the project run the migrations with the following command: rails db:migrate
- Run the project with the following command: rails s
- To consume the services you can access through Postman or directly from the swagger in the route /api-docs

## Database relational model
![Database relational model](assets/relational.jpg)