require "test_helper"

class ContactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact = contacts(:one)
  end

  test "should get index" do
    get contacts_url, as: :json
    assert_response :success
  end

  test "should create contact" do
    assert_difference("Contact.count") do
      post contacts_url, params: { contact: { address: @contact.address, birthdate: @contact.birthdate, city_fk: @contact.city_fk, description: @contact.description, email: @contact.email, gender_fk: @contact.gender_fk, house_type: @contact.house_type, last_name: @contact.last_name, name: @contact.name } }, as: :json
    end

    assert_response :created
  end

  test "should show contact" do
    get contact_url(@contact), as: :json
    assert_response :success
  end

  test "should update contact" do
    patch contact_url(@contact), params: { contact: { address: @contact.address, birthdate: @contact.birthdate, city_fk: @contact.city_fk, description: @contact.description, email: @contact.email, gender_fk: @contact.gender_fk, house_type: @contact.house_type, last_name: @contact.last_name, name: @contact.name } }, as: :json
    assert_response :success
  end

  test "should destroy contact" do
    assert_difference("Contact.count", -1) do
      delete contact_url(@contact), as: :json
    end

    assert_response :no_content
  end
end
